package com.calderaminecraft.calderahide;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

class Game {
    private CalderaHide plugin;
    String title;
    String seekerTitle = "Seekers";
    String hiderTitle = "Hiders";

    private ItemStack seekerItem;
    private ItemStack hiderItem;

    List<ItemStack> seekerKit = new ArrayList<>();
    List<ItemStack> hiderKit = new ArrayList<>();

    Location respawnPoint;
    Location spectatorPoint;
    Location seekerPoint;

    long seekerDelay;
    long originalSeekerDelay;
    long timer;
    private long originalTimer;

    int totalSeekers = 0;
    int totalHiders = 0;
    int lightningAfter;
    int lightningEvery;

    private BukkitTask task;
    boolean saveSettings = false;
    boolean started = false;
    private boolean seekersReleased = false;

    Map<UUID, String> players = new HashMap<>();

    Game(String gameTitle, CalderaHide p) {
        title = gameTitle;
        plugin = p;

        setItem("seekers", new ItemStack(Material.TNT));
        setItem("hiders", new ItemStack(Material.PLAYER_HEAD));
    }

    void start() {
        originalTimer = timer;
        seekerDelay = (System.currentTimeMillis() + originalSeekerDelay);
        for (Map.Entry<UUID, String> entry : players.entrySet()) {
            UUID uuid = entry.getKey();
            String role = entry.getValue();

            Player player = Bukkit.getPlayer(uuid);
            if (role.equals("hiders")) {
                if (hiderKit != null) {
                    for (ItemStack item : hiderKit) player.getInventory().addItem(item);
                    player.updateInventory();
                    player.getInventory().setHelmet(hiderItem);
                }
                player.teleport(respawnPoint);
            }
            if (role.equals("seekers")) {
                if (seekerKit != null) {
                    for (ItemStack item : seekerKit) player.getInventory().addItem(item);
                    player.updateInventory();
                    player.getInventory().setHelmet(seekerItem);
                }
                if (seekerPoint != null) player.teleport(seekerPoint);
            }
        }

        task = new BukkitRunnable() {
            @Override
            public void run() {
                timer -= 1000;
                long minutes = ((timer / 1000) / 60);
                long seconds = (timer / 1000);
                if (timer <= 0) {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "The " + seekerTitle + " have run out of time! " +
                            totalHiders + " " + hiderTitle + " have won.");
                    this.cancel();
                    endGame(saveSettings);
                    return;
                }

                if (seekerDelay <= System.currentTimeMillis() && !seekersReleased) {
                    for (Map.Entry<UUID, String> entry : players.entrySet()) {
                        UUID uuid = entry.getKey();
                        String role = entry.getValue();

                        Player player = Bukkit.getPlayer(uuid);
                        if (role.equals("seekers") && seekerKit != null) {
                            player.teleport(respawnPoint);
                        }
                    }
                    seekersReleased = true;
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + seekerTitle + " have been released!");
                }

                if ((timer % 60000) == 0) {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "There is only " + plugin.convertTime(timer) + " remaining!");
                } else if ((timer == 30000)) {
                     Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "There is only 30 seconds remaining!");
                } else if ((timer == 20000)) {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "There is only 20 seconds remaining!");
                } else if ((timer == 10000)) {
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "There is only 10 seconds remaining!");
                }

                if (lightningAfter > 0) {
                    if (seconds == (lightningAfter * 60)) {
                        Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "The remaining " + hiderTitle +
                                " will now be struck by lightning every " + lightningEvery + " seconds.");
                    }
                    if ((minutes < lightningAfter) && (seconds % lightningEvery) == 0) {
                        //Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + hiderTitle + " have been struck by lightning!");
                        for (Map.Entry<UUID, String> entry : players.entrySet()) {
                            UUID uuid = entry.getKey();
                            String role = entry.getValue();
                            if (role.equals("seekers")) continue;
                            Player player = Bukkit.getPlayer(uuid);
                            respawnPoint.getWorld().strikeLightningEffect(player.getLocation());
                        }
                    }
                }
            }
        }.runTaskTimer(plugin, 0, 1 * 20);

        Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + title + " has started! There are " +
                totalSeekers + " " + seekerTitle + " and " + totalHiders + " " + hiderTitle + ", with " + plugin.convertTime(timer) + " remaining!");
        if (seekerPoint != null) {
            Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + seekerTitle + " will be released in " + plugin.convertTime(originalSeekerDelay));
        }
        started = true;
    }

    void removePlayer(UUID uuid, String type) {
        if (!players.containsKey(uuid)) return;
        Player player = Bukkit.getPlayer(uuid);
        if (player != null) {
            if (type.equals("logout")) {
                /*World spawn = Bukkit.getWorld(plugin.getConfig().getString("spawn"));
                player.getInventory().clear();
                player.teleport(spawn.getSpawnLocation());*/
                players.remove(uuid);
            }
            if (type.equals("death")) {
                String role = players.get(uuid);
                if (role.equals("hiders")) {
                    players.remove(uuid);
                    players.put(uuid, "seekers");
                    player.teleport(respawnPoint);
                    player.setHealth(20);
                    player.getInventory().clear();
                    for (ItemStack item : seekerKit) player.getInventory().addItem(item);
                    player.updateInventory();
                    player.getInventory().setHelmet(seekerItem);
                    totalHiders--;
                    totalSeekers++;
                    if (totalHiders > 0 && started) {
                        Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "There are only " + totalHiders + " " + hiderTitle + " remaining!");
                    }
                } else {
                    player.getInventory().clear();
                    player.teleport(spectatorPoint);
                    player.setHealth(20);
                }
            }
            if (started && totalHiders < 1) {
                endGame(saveSettings);
                return;
            }
            if (!started) {
                Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + player.getDisplayName() + ChatColor.GOLD + " has left the game of " + title);
            }
        }

        if (players.get(uuid).equals("hiders")) {
            totalHiders--;
            if (totalHiders > 0 && started) {
                Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "There are only " + totalHiders + " " + hiderTitle + " remaining!");
            }
        }
        if (players.get(uuid).equals("seekers")) totalSeekers--;
        players.remove(uuid);

        if ((totalHiders < 1 || totalSeekers < 1) && started) endGame(saveSettings);
    }

    void endGame(boolean reset) {
        if (totalHiders < 1 && started) {
            Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "All of the " + hiderTitle + " have been killed! The " + seekerTitle + " win!");
        }
        if (totalSeekers < 1 && started) {
            Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "The " + seekerTitle + " have all died! The " + hiderTitle + " win!");
        }

        for (Map.Entry<UUID, String> entry : players.entrySet()) {
            UUID uuid = entry.getKey();
            Player player = Bukkit.getPlayer(uuid);
            player.getInventory().clear();
            player.teleport(respawnPoint);
        }

        if (started) task.cancel();
        if (!reset) {
            plugin.activeGame = null;
        } else {
            for (Map.Entry<UUID, String> entry : players.entrySet()) {
                UUID uuid = entry.getKey();

                Player player = Bukkit.getPlayer(uuid);
                player.getInventory().clear();
                player.teleport(spectatorPoint);
                players.replace(uuid, "hiders");
            }

            started = false;
            seekersReleased = false;
            timer = originalTimer;
        }
    }

    void countPlayers() {
        totalSeekers = 0;
        totalHiders = 0;
        for (Map.Entry<UUID, String> entry : players.entrySet()) {
            String role = entry.getValue();

            if (role.equals("hiders")) totalHiders++;
            if (role.equals("seekers")) totalSeekers++;
        }
    }

    void setItem(String team, ItemStack item) {
        item.addUnsafeEnchantment(Enchantment.BINDING_CURSE, 1);
        ItemMeta meta = item.getItemMeta();
        meta.hasItemFlag(ItemFlag.HIDE_ENCHANTS);
        item.setItemMeta(meta);
        if (team.equalsIgnoreCase("seekers")) seekerItem = item;
        if (team.equalsIgnoreCase("hiders")) hiderItem = item;
    }

    String getTeamName(String team) {
        if (team.equalsIgnoreCase("seekers")) return seekerTitle;
        if (team.equalsIgnoreCase("hiders")) return hiderTitle;
        return "";
    }
}
