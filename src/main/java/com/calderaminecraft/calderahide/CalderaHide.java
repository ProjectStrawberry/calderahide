package com.calderaminecraft.calderahide;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class CalderaHide extends JavaPlugin {
    Game activeGame;

    @Override
    public void onEnable() {
        CalderaHide plugin = this;
        plugin.getConfig().options().copyDefaults(true);
        plugin.saveConfig();

        String spawnWorld = plugin.getConfig().getString("spawn");
        if (Bukkit.getWorld(spawnWorld) == null) {
            getLogger().severe("World \"" + spawnWorld + "\" does not exist!");
        }

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);
        this.getCommand("calderahide").setExecutor(new CommandCalderaHide(this));

        getLogger().info("CalderaHide loaded.");
    }

    long stringToTime(String time) {
        String validModifiers = "smhd";
        char timeModifier = time.charAt(time.length() - 1);
        int timeNumber;
        long timeDuration = 0;
        if (validModifiers.indexOf(timeModifier) < 0) {
            return 0;
        }

        try {
            timeNumber = Integer.parseInt(time.substring(0, time.length() - 1));
        } catch (NumberFormatException e) {
            return 0;
        }


        if (timeModifier == 's') {
            timeDuration = timeNumber * 1000L;
        }
        if (timeModifier == 'm') {
            timeDuration = timeNumber * 60 * 1000L;
        }
        if (timeModifier == 'h') {
            timeDuration = timeNumber * 60 * 60 * 1000L;
        }
        if (timeModifier == 'd') {
            timeDuration = timeNumber * 24 * 60 * 60 * 1000L;
        }

        return timeDuration;
    }

    String convertTime(long ms) {
        long seconds = (ms / 1000) % 60L;
        long minutes = (ms / (1000 * 60)) % 60L;
        long hours = (ms / (1000 * 60 * 60)) % 24L;
        long days = (ms / (1000 * 60 * 60 * 24L));

        String reply = "";
        if (days > 0) {
            reply += days + (days == 1 ? " day " : " days ");
        }
        if (hours > 0) {
            reply += hours + (hours == 1 ? " hour " : " hours ");
        }
        if (minutes > 0) {
            reply += minutes + (minutes == 1 ? " minute " : " minutes ");
        }
        if (seconds > 0) {
            reply += seconds + (seconds == 1 ? " second " : " seconds ");
        }

        return reply.trim();
    }

    List<BaseComponent> pageify(List messages, int pageNumber, String pageCmd) {
        int pageSize = 8;
        List<BaseComponent> page = new ArrayList<>();

        int totalPages = messages.size() / pageSize + ((messages.size() % pageSize == 0) ? 0 : 1);
        if (totalPages < pageNumber) pageNumber = totalPages;

        int count = 0;
        int curPageNum;

        page.add(new TextComponent(ChatColor.DARK_GREEN + "Page " + ChatColor.GOLD + pageNumber + ChatColor.DARK_GREEN + " of " + ChatColor.GOLD + totalPages));

        for (Object curPage : messages) {
            count++;
            curPageNum = count / pageSize + ((count % pageSize == 0) ? 0 : 1);
            if (curPageNum != pageNumber) continue;
            if (!(curPage instanceof BaseComponent)) curPage = new TextComponent((String) curPage);
            page.add((BaseComponent) curPage);
        }

        TextComponent footer = new TextComponent();
        TextComponent backButton = new TextComponent(ChatColor.DARK_GREEN + "[Last Page]");
        if (pageNumber > 1) backButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, pageCmd + " " + (pageNumber - 1)));
        TextComponent nextButton = new TextComponent(ChatColor.DARK_GREEN + "[Next Page]");
        if (pageNumber < totalPages) nextButton.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, pageCmd + " " + (pageNumber + 1)));

        footer.addExtra(backButton);
        footer.addExtra(" | ");
        footer.addExtra(nextButton);

        page.add(footer);

        return page;
    }
}
