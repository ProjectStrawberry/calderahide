package com.calderaminecraft.calderahide;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class CommandCalderaHide implements TabExecutor {
    private CalderaHide plugin;

    CommandCalderaHide(CalderaHide p) {
        plugin = p;
    }
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            args = new String[]{"help"};
        }

        if (args[0].equals("help") || args[0].equals("?")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            int pageNum = 1;
            if (args.length > 1) {
                try {
                    pageNum = Integer.parseInt(args[1]);
                    if (pageNum < 1) pageNum = 1;
                } catch (NumberFormatException ignored) {}
            }

            List<TextComponent> messages = new ArrayList<>();
            messages.add(formatText("/calderahide join - Joins a game of hide and seek."));
            messages.add(formatText("/calderahide leave - Leaves a game of hide and seek."));
            if (sender.hasPermission("calderahide.admin")) {
                messages.add(formatText("/calderahide create <name (ex: Hunters vs Turkeys)> - Creates a new game of hide and seek."));
                messages.add(formatText("/calderahide start - Starts the game."));
                messages.add(formatText("/calderahide end - Forcibly ends a game."));
                messages.add(formatText("/calderahide save - Toggles whether game settings should be saved when a game finishes."));
                messages.add(formatText("/calderahide setname <team> <display name> - Sets the display name for the hiders or the seekers."));
                messages.add(formatText("/calderahide setrespawn - Sets the respawn point to your current location."));
                messages.add(formatText("/calderahide setspectator - Sets the spectator point to your current location."));
                messages.add(formatText("/calderahide setseekerpoint <delay (1s/m)>"));
                messages.add(formatText("/calderahide setblock <team> - Sets the item in your hand as the block that identifies a team."));
                messages.add(formatText("/calderahide createkit <team> - Sets the kit for the specified team to match the items in your inventory."));
                messages.add(formatText("/calderahide setteam <team> <player> - Sets a players team."));
                messages.add(formatText("/calderahide settimer <time> - Sets the timer on the game."));
                messages.add(formatText("/calderahide setlightning <timeremaining (minutes)> <frequency (seconds)> - Causes lightning to strike all remaining hiders."));
            }

            List<BaseComponent> pages = plugin.pageify(messages, pageNum, "/calderahide help");

            for (BaseComponent page : pages) player.spigot().sendMessage(page);

            return true;
        }

        if (args[0].equals("create")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame != null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is already an active game of hide and seek.");
                return true;
            }
            String title = "";
            if (args.length > 1) {
                for (int i = 1; i < args.length; i++) title += args[i] + " " ;
            }

            plugin.activeGame = new Game((title.equals("") ? "Hide and Seek" : title), plugin);
            sender.sendMessage(ChatColor.DARK_GREEN + "You've created a new game of " + (title.equals("") ? "Hide and Seek" : title));
            return true;
        }

        if (args[0].equals("save")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is not an active game of hide and seek.");
                return true;
            }

            plugin.activeGame.saveSettings = !plugin.activeGame.saveSettings;

            sender.sendMessage(ChatColor.DARK_GREEN + "The hide and seek settings will " + (plugin.activeGame.saveSettings ? " be saved" : " no longer be saved") + " when the game ends.");
            return true;
        }

        if (args[0].equals("end")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek to start.");
                return true;
            }

            String title = plugin.activeGame.title;

            plugin.activeGame.endGame(false);

            Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + "The game of " + title + " has been forcibly ended.");
            return true;
        }

        if (args[0].equals("start")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek to start.");
                return true;
            }

            if (plugin.activeGame.started) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This game has already started.");
                return true;
            }

            if (plugin.activeGame.players.size() < 2) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is not enough players to start this game.");
                return true;
            }

            if (plugin.activeGame.totalSeekers < 1) {
                sender.sendMessage(ChatColor.RED + "Error: "  + ChatColor.DARK_RED + "You must set at least one player as a seeker before you can start this game.");
                return true;
            }

            if (plugin.activeGame.totalHiders < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There needs to be at least one hider.");
                return true;
            }

            if (plugin.activeGame.respawnPoint == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You must set a respawn point before this game can start.");
                return true;
            }

            if (plugin.activeGame.timer < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You must set a timer before this game can start.");
                return true;
            }

            plugin.activeGame.start();
            return true;
        }

        if (args[0].equals("join")) {
            if (!sender.hasPermission("calderahide.use")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek to join.");
                return true;
            }

            Player player = (Player) sender;

            if (plugin.activeGame.players.containsKey(player.getUniqueId())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You've already joined this game.");
                return true;
            }

            player.getInventory().clear();
            plugin.activeGame.players.put(player.getUniqueId(), (plugin.activeGame.started ? "seekers" : "hiders"));
            plugin.activeGame.countPlayers();

            sender.sendMessage(ChatColor.DARK_GREEN + "You've joined this game of " + plugin.activeGame.title);
            Bukkit.broadcastMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.GOLD + player.getDisplayName() + ChatColor.GOLD + " has joined the game of " + plugin.activeGame.title);
            return true;
        }

        if (args[0].equals("leave")) {
            if (!sender.hasPermission("calderahide.use")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek to join.");
                return true;
            }

            Player player = (Player) sender;

            if (!plugin.activeGame.players.containsKey(player.getUniqueId())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You have not joined this game.");
                return true;
            }

            plugin.activeGame.removePlayer(player.getUniqueId(), "quit");

            sender.sendMessage(ChatColor.DARK_GREEN + "You've left the game of " + plugin.activeGame.title);
            return true;
        }

        if (args[0].equals("setname")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderahide setname <team> <display name>");
                return true;
            }

            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek.");
                return true;
            }

            String team = args[1];
            String displayName = args[2];

            if (team.equalsIgnoreCase("seekers")) {
                plugin.activeGame.seekerTitle = displayName;
                sender.sendMessage(ChatColor.DARK_GREEN + "The seekers display name has been changed to " + ChatColor.GOLD + displayName);
                return true;
            }

            if (team.equalsIgnoreCase("hiders")) {
                plugin.activeGame.hiderTitle = displayName;
                sender.sendMessage(ChatColor.DARK_GREEN + "The hiders display name has been changed to " + ChatColor.GOLD + displayName);
                return true;
            }

            sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid team.");
            return true;
        }

        if (args[0].equals("setlightning")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek.");
                return true;
            }
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderahide setlightning <timeremaining> <frequency>");
                return true;
            }

            int timeRemaining;
            int frequency;

            try {
                timeRemaining = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid number for <timeremaining>.");
                return true;
            }

            try {
                frequency = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid number for <frequency>.");
                return true;
            }

            plugin.activeGame.lightningAfter = timeRemaining;
            plugin.activeGame.lightningEvery = frequency;

            sender.sendMessage(ChatColor.DARK_GREEN + "Lightning will strike all hiders every " + frequency + " seconds when there is less than " + timeRemaining + " minutes left.");

            return true;
        }

        if (args[0].equals("setrespawn")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            plugin.activeGame.respawnPoint = player.getLocation();

            sender.sendMessage(ChatColor.DARK_GREEN + "The respawn point has been set to your current location.");
            return true;
        }

        if (args[0].equals("setspectator")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            Player player = (Player) sender;

            plugin.activeGame.spectatorPoint = player.getLocation();

            sender.sendMessage(ChatColor.DARK_GREEN + "The spectator point has been set to your current location.");
            return true;
        }

        if (args[0].equals("setseekerpoint")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderahide setseekerpoint <delay (1s/m)>");
                return true;
            }

            Player player = (Player) sender;
            long delay = plugin.stringToTime(args[1]);
            if (delay < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid delay.");
                return true;
            }

            plugin.activeGame.seekerPoint = player.getLocation();
            plugin.activeGame.originalSeekerDelay = delay;
            plugin.activeGame.seekerDelay = delay;

            sender.sendMessage(ChatColor.DARK_GREEN + "The seeker point has been set to your current location.");
            return true;
        }

        if (args[0].equals("setblock")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderahide setblock <team>");
                return true;
            }

            String team = args[1];

            if (!team.equalsIgnoreCase("seekers") && !team.equalsIgnoreCase("hiders")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid team.");
                return true;
            }

            Player player = (Player) sender;

            ItemStack item = player.getInventory().getItemInMainHand();

            if (item == null || item.getType().equals(Material.AIR)) {
                player.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You're not holding an item.");
                return true;
            }

            ItemStack clonedItem = item.clone();
            clonedItem.setAmount(1);

            plugin.activeGame.setItem(team, clonedItem);

            sender.sendMessage(ChatColor.DARK_GREEN + "You've set the head item for the " + ChatColor.GOLD + plugin.activeGame.getTeamName(team));
            return true;
        }

        if (args[0].equals("createkit")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek.");
                return true;
            }
            if (!(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "This command does not work from the console.");
                return true;
            }

            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderahide setblock <team>");
                return true;
            }

            String team = args[1];

            if (!team.equalsIgnoreCase("seekers") && !team.equalsIgnoreCase("hiders")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid team.");
                return true;
            }

            Player player = (Player) sender;

            player.getInventory().setHelmet(null);
            if (team.equalsIgnoreCase("seekers")) {
                plugin.activeGame.seekerKit = new ArrayList<>();
                ItemStack[] inv = player.getInventory().getContents();
                for (ItemStack item : inv) {
                    if (item == null) continue;
                    plugin.activeGame.seekerKit.add(item.clone());
                }
            }
            if (team.equalsIgnoreCase("hiders")) {
                plugin.activeGame.hiderKit = new ArrayList<>();
                ItemStack[] inv = player.getInventory().getContents();
                for (ItemStack item : inv) {
                    if (item == null) continue;
                    plugin.activeGame.hiderKit.add(item.clone());
                }
            }

            sender.sendMessage(ChatColor.DARK_GREEN + "You've set the kit for the " + ChatColor.GOLD + plugin.activeGame.getTeamName(team));
            return true;
        }

        if (args[0].equals("setteam")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek.");
                return true;
            }
            if (args.length < 3) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderahide setteam <team> <player>");
                return true;
            }

            String team = args[1];
            Player player = Bukkit.getPlayer(args[2]);

            if (!team.equalsIgnoreCase("seekers") && !team.equalsIgnoreCase("hiders")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid team.");
                return true;
            }

            if (player == null || !player.isOnline()) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player is not online.");
                return true;
            }

            if (!plugin.activeGame.players.containsKey(player.getUniqueId())) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That player has not joined the game.");
                return true;
            }

            plugin.activeGame.players.replace(player.getUniqueId(), team.toLowerCase());
            plugin.activeGame.countPlayers();

            sender.sendMessage(ChatColor.DARK_GREEN + "You've assigned that player to the " + ChatColor.GOLD + plugin.activeGame.getTeamName(team) + ChatColor.DARK_GREEN + " team.");
            player.sendMessage(ChatColor.DARK_GREEN + "You've been assigned to the " + ChatColor.GOLD + plugin.activeGame.getTeamName(team) + ChatColor.DARK_GREEN + " team.");

            return true;
        }

        if (args[0].equals("settimer")) {
            if (!sender.hasPermission("calderahide.admin")) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "You do not have permission to use this command.");
                return true;
            }
            if (plugin.activeGame == null) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "There is no active game of hide and seek.");
                return true;
            }
            if (args.length < 2) {
                sender.sendMessage(ChatColor.DARK_GREEN + "Usage: /calderahide settimer <time>");
                return true;
            }

            long time = plugin.stringToTime(args[1]);
            if (time < 1) {
                sender.sendMessage(ChatColor.RED + "Error: " + ChatColor.DARK_RED + "That is not a valid time.");
                return true;
            }

            plugin.activeGame.timer = time;

            sender.sendMessage(ChatColor.DARK_GREEN + "The timer has been set to " + ChatColor.GOLD + plugin.convertTime(time));
            return true;
        }

        Bukkit.dispatchCommand(sender, "calderahide help");
        return true;
    }

    private TextComponent formatText(String message) {
        TextComponent component = new TextComponent(message);
        component.setColor(ChatColor.GOLD.asBungee());
        return component;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        List<String> tabComplete = new ArrayList<>();
        if (args.length == 1) {
            tabComplete.add("join");
            tabComplete.add("leave");
            if (sender.hasPermission("calderahide.admin")) {
                tabComplete.add("start");
                tabComplete.add("end");
                tabComplete.add("setname");
                tabComplete.add("setrespawn");
                tabComplete.add("setspectator");
                tabComplete.add("setseekerpoint");
                tabComplete.add("setblock");
                tabComplete.add("createkit");
                tabComplete.add("setteam");
                tabComplete.add("settimer");
                tabComplete.add("setlightning");
            }
        }
        if (args.length == 2 && sender.hasPermission("calderahide.admin")) {
            if (args[0].equals("setname") || args[0].equals("setblock") || args[0].equals("createkit") || args[0].equals("setteam")) {
                tabComplete.add("seekers");
                tabComplete.add("hiders");
            }
            if (args[0].equals("settimer")) tabComplete.add("<time>");
            if (args[0].equals("setlightning")) tabComplete.add("<time remaining>");
            if (args[0].equals("setseekerpoint")) tabComplete.add("<delay (1s/m)>");
        }
        if (args.length == 3 && sender.hasPermission("calderahide.admin")) {
            if (args[0].equals("setname")) tabComplete.add("<display name>");
            if (args[0].equals("setlightning")) tabComplete.add("<frequency>");
            if (args[0].equals("setteam")) return null;
        }
        return (args.length > 0) ? StringUtil.copyPartialMatches(args[args.length - 1], tabComplete, new ArrayList<>()) : null;
    }
}
