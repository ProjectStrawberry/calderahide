package com.calderaminecraft.calderahide;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EventListeners implements Listener {
    private CalderaHide plugin;
    private List<UUID> sendToSpawn = new ArrayList<>();
    private World spawn = Bukkit.getWorld(plugin.getConfig().getString("spawn"));

    EventListeners(CalderaHide p) {
        plugin = p;
    }

    @EventHandler
    public void PlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (sendToSpawn.contains(player.getUniqueId())) player.teleport(spawn.getSpawnLocation());
    }

    @EventHandler
    public void PlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (plugin.activeGame == null) return;
        if (plugin.activeGame.players.containsKey(player.getUniqueId())) {
            plugin.activeGame.removePlayer(player.getUniqueId(), "logout");
            sendToSpawn.add(player.getUniqueId());
        }
    }

    @EventHandler
    public void PlayerChangedWorldEvent(PlayerChangedWorldEvent event) {
        Player player = event.getPlayer();
        if (plugin.activeGame == null) return;
        if (plugin.activeGame.players.containsKey(player.getUniqueId())) {
            plugin.activeGame.removePlayer(player.getUniqueId(), "worldchange");
        }
    }

    @EventHandler
    public void EntityDamageEvent(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if (plugin.activeGame == null) return;

        Player player = (Player) event.getEntity();
        if (!plugin.activeGame.players.containsKey(player.getUniqueId())) return;
        if (event.getDamage() >= player.getHealth()) {
            plugin.activeGame.removePlayer(player.getUniqueId(), "death");
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void EntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        if (plugin.activeGame == null) return;

        Player player = (Player) event.getEntity();
        if (!plugin.activeGame.players.containsKey(player.getUniqueId())) return;
        if ((event.getDamager() instanceof Player)) {
            Player attacker = (Player) event.getDamager();
            if (!plugin.activeGame.players.containsKey(attacker.getUniqueId())) return;

            if (plugin.activeGame.players.get(player.getUniqueId()).equals(plugin.activeGame.players.get(attacker.getUniqueId()))) {
                event.setCancelled(true);
                attacker.sendMessage(ChatColor.DARK_RED + "[Caldera] " + ChatColor.RED + "You can not attack people on your team!");
                return;
            }
        }

        if (event.getDamage() >= player.getHealth()) {
            plugin.activeGame.removePlayer(player.getUniqueId(), "death");
            event.setCancelled(true);
        }
    }

    @EventHandler (priority= EventPriority.LOWEST)
    public void playerDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        if (plugin.activeGame == null) return;
        if (!plugin.activeGame.players.containsKey(player.getUniqueId())) return;

        event.setKeepInventory(true);
        event.getDrops().clear();
        player.getInventory().clear();
        plugin.activeGame.removePlayer(player.getUniqueId(), "death");
    }
}
